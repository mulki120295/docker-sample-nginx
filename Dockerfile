FROM nginx:alpine

ADD ./ /var/www/html

COPY default.conf /etc/nginx/conf.d/
COPY index.html /usr/share/nginx/html/

WORKDIR /var/www/html